import argparse
import csv
import os
import time
import json
from pymediainfo import MediaInfo


# output file
FILE_NAME = 'mediadata.csv'

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--folder", required=True,
                help="Topfolder to read media data from")
args = vars(ap.parse_args())
arg_folder = args["folder"]

#get list of all (sub)directories under the given folder
#os.walk returns a 3-tuple (dirpath, dirnames, filenames).
dir_list = [x for x in os.walk(arg_folder)]

# list container for all media dictionaries
file_info = []

# if performer not in media info, get it from artist folder name
def getPerformerFromFoldername(foldername):
    perf_folder = os.path.basename(os.path.dirname(foldername))

    performer = ''

    if "(" and ")" in perf_folder:
        start = perf_folder.find("(")
        end = perf_folder.find(")")
        sub = perf_folder[start:end + 1]
        performer = perf_folder.replace(sub, '').strip()

    if "," in performer:
        names = performer.split(",")
        performer = f'{names[1].strip()} {names[0].strip()}'

    return performer


def getAlbumnameFromFoldername(foldername):
    alb_folder = os.path.basename(foldername)
    name = alb_folder[alb_folder.rfind("-") +1 :]
    name = name.strip()

    return name


def getDurationFromMilliseconds(ms):
    return time.strftime("%M:%S", time.gmtime(ms/1000))

# for every subdir and every file in that subdir read the media info
for dir in dir_list:
    # if the directory has files in it // dir[2] are files in the directory
    if dir[2]:
        # for every file in the directory
        for file in dir[2]:
            filepath = os.path.join(dir[0], file)

            dir_name = os.path.basename(os.path.dirname(filepath))

            # initialize values
            full_folder_name = ''
            performer = ''
            album_name = ''
            bitrate = ''
            track_title = ''
            track_position = ''
            druation = 0

            # only lookup mediainfo for mp3 ,flac, ogg and wav files
            if os.path.splitext(filepath)[1] in ['.mp3', '.flac', '.ogg', '.wav', '.wma']:

                # read the actual media info from the file
                m_info = MediaInfo.parse(filepath)
                m_json = json.loads(m_info.to_json())
                for track in m_json['tracks']:
                    if track['track_type'] == 'General':
                        if 'folder_name' in track:
                            full_folder_name = track['folder_name']

                        if 'performer' in track:
                            performer = track['performer']
                        else:
                            performer = getPerformerFromFoldername(track['folder_name'])

                        if 'album' in track:
                            album_name = track['album']
                        else:
                            album_name = os.path.basename(track['folder_name'])

                        if 'overall_bit_rate' in track:
                            bitrate = track['overall_bit_rate']

                        if 'track_name' in track:
                            track_title = track['track_name']
                        else:
                            track_title = os.path.splitext(file)[0]

                        if 'track_name_position' in track:
                            track_position = track['track_name_position']

                        if 'duration' in track:
                            duration = getDurationFromMilliseconds(track['duration'])

                # create dict for each file
                data = {
                    'folder': full_folder_name,
                    'interpret': performer,
                    'album': f"{album_name} @{bitrate}",
                    'title': track_title,
                    'title-nr': track_position,
                    'duration': duration,
                    'type': os.path.splitext(filepath)[1],
                }

                # append dict to general list
                file_info.append(data)


# write list to csv file
csv_columns = ['folder', 'interpret', 'album', 'title', 'title-nr', 'duration', 'type']
try:
    with open(FILE_NAME, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, delimiter=";", fieldnames=csv_columns)
        # write csv_colums as header
        writer.writeheader()
        # write one row for each dataset in file_info
        for d in file_info:
            writer.writerow(d)
except IOError:
    print("I/O error")

